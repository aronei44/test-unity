using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Driver : MonoBehaviour
{
    [SerializeField] float steerSpeed = 200;
    [SerializeField] float carSpeed = 15;
    [SerializeField] float slowSpeed = 10;
    [SerializeField] float boostSpeed = 20;
    float realSpeed = 0;
    void Start()
    {
        realSpeed = carSpeed;
    }

    void Update()
    {
        float steerAmount = Input.GetAxis("Horizontal") * steerSpeed * Time.deltaTime;
        float carAmount = Input.GetAxis("Vertical") * realSpeed * Time.deltaTime;
        transform.Rotate(0,0,-steerAmount);   
        transform.Translate(0, carAmount, 0);
    }
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Boost")
        {
            Debug.Log("boost");
            realSpeed = boostSpeed;
        }
        else if (other.tag == "Bump")
        {
            Debug.Log("bump");
            realSpeed = slowSpeed;
        } else {
            realSpeed = carSpeed;
        }
    }
}
